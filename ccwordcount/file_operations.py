import os
import sys

if len(sys.argv) > 3:
    print("Please Enter valid options arg length is beyond 3 or 1")


def word_count(filename):
    with open(filename, mode='r') as fb:
        txt_lines = fb.readlines()
        counted_words = 0
        for line in enumerate(txt_lines):
            counted_words += len(line[1].split())
    return counted_words


# Needs to be worked upon - supposed to be displaying - 339292 but displaying - 332147
def character_count(filename):
    with open(filename, mode='r') as file:
        content = file.read()
        counted_character = len(content)
        return counted_character


if len(sys.argv) == 3:
    if sys.argv[1] in ['-c', '-l', '-w', '-m']:
        file_name = sys.argv[2]
        options = sys.argv[1]
        if os.path.isfile(file_name):
            if options == '-c':
                file_size = os.stat(file_name)
                print(f'{file_size.st_size} {file_name}')
            elif options == '-l':
                with open(file_name, mode='r') as rl:
                    lines = len(rl.readlines())
                    print(f'{lines} {file_name}')
            elif options == '-w':
                word_c = word_count(file_name)
                print(f'{word_c} {file_name}')
            elif options == '-m':
                character_c = character_count(file_name)
                print(f'{character_c} {file_name}')

        else:
            print('Please enter a valid file name')
    else:
        print("Please enter a valid option")

if len(sys.argv) == 2:
    file_name = sys.argv[1]
    if os.path.isfile(file_name):
        with open(file_name, mode='r') as rl:
            lines = len(rl.readlines())
            print(f'{lines}', end=' ')
        word_c = word_count(file_name)
        print(f'{word_c}', end=' ')
        file_size = os.stat(file_name)
        print(f'{file_size.st_size} {file_name}')
    else:
        print("Please enter a valid file name")

if len(sys.argv) == 1:
    file_name = 'test.txt'
    if os.path.isfile(file_name):
        with open(file_name, mode='r') as rl:
            lines = len(rl.readlines())
            print(f'{lines}', end=' ')
        word_c = word_count(file_name)
        print(f'{word_c}', end=' ')
        file_size = os.stat(file_name)
        print(f'{file_size.st_size}')
