with open('tests/step1/valid.json', 'r') as fd:
    data = fd.read()
    if data:
        lexical = []
        for char in data:
            if char == '{':
                lexical.append(char)
            elif char == '}':
                lexical.pop()
        if len(lexical) == 0:
            print("exit 0")
            exit(0)
        else:
            print("1")
            exit(1)
    else:
        print("exit 1")
        exit(1)
